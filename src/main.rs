use std::path::Path;
use indicatif::ProgressStyle;
use image::Luma;
use image::DynamicImage;
use num::complex::Complex;
use image::GrayImage;
use image::io::Reader as ImageReader;
use image::imageops::FilterType;

use indicatif::ProgressBar;

const DIM: u32 = 100;
const FREQ_CUTOFF: u32 = 100;
const EXPLODER_CUTOFF: u32 = 100;
const SCALE_FACTOR: u32 = 4;
const INPUT_FILE: &'static str = "test.png";
const INPUT_FILE_DOWNSAMPLED: &'static str = "test-downsampled.png";

fn forward_ft(coefficients: &Vec<Vec<Complex<f32>>>, cutoff: (u32, u32)) -> Vec<Vec<Complex<f32>>> {
  let n_x = coefficients.len() as u32;
  let n_y = coefficients[0].len() as u32;

  let bar = ProgressBar::new((n_x * n_y) as u64);
  bar.set_message("Computing {x} -> {k}");
  bar.set_style(ProgressStyle::default_bar()
    .template("{msg} {bar:40.cyan/blue} [{eta} remaining]")
    .progress_chars("##-"));

  let r = (0..n_x).into_iter().map(|kx| {
    (0..n_y).into_iter().map(|ky| {
      bar.inc(1);

      if kx > cutoff.0 || ky > cutoff.1 {
        Complex::new(0.0, 0.0)
      } else {
        let fac_kx = 2.0 * 3.14159 / (n_x as f32) * (kx as f32);
        let fac_ky = 2.0 * 3.14159 / (n_y as f32) * (ky as f32);

        let mut sum = Complex::new(0.0, 0.0);

        for x in 0..n_x {
          for y in 0..n_y {
            let f = coefficients[x as usize][y as usize];
            sum += f * Complex::new(0.0, - fac_kx * (x as f32) - fac_ky * (y as f32)).exp();
          }
        }

        sum
      }
    }).collect()
  }).collect();
  bar.finish();
  r
}

fn reverse_ft(coefficients: &Vec<Vec<Complex<f32>>>) -> Vec<Vec<Complex<f32>>> {
  let n_x = coefficients.len();
  let n_y = coefficients[0].len();

  let bar = ProgressBar::new((n_x * n_y) as u64);
  bar.set_message("Computing {k} -> {x}");
  bar.set_style(ProgressStyle::default_bar()
    .template("{msg} {bar:40.cyan/blue} [{eta} remaining]")
    .progress_chars("##-"));
  let r = (0..n_x).into_iter().map(|x| {
    (0..n_y).into_iter().map(|y| {
      bar.inc(1);
      let fac_x = 2.0 * 3.14159 / (n_x as f32) * (x as f32);
      let fac_y = 2.0 * 3.14159 / (n_y as f32) * (y as f32);

      let mut sum = Complex::new(0.0, 0.0);

      for kx in 0..n_x {
        for ky in 0..n_y {
          let f = coefficients[kx as usize][ky as usize];
          sum += f * Complex::new(0.0, fac_x * (kx as f32) + fac_y * (ky as f32)).exp();
        }
      }

      sum / ((n_x * n_y) as f32)
    }).collect()
  }).collect();
  bar.finish();
  r
}

fn img_to_pixels(img: &GrayImage) -> Vec<Vec<f32>> {
  let n_x = img.width();
  let n_y = img.height();

  (0..n_x).into_iter().map(|x| {
    (0..n_y).into_iter().map(|y| {
      img.get_pixel(x, y).0[0] as f32 / 256.0
    }).collect()
  }).collect()
}

fn pixels_to_img(values: Vec<Vec<u8>>, scale_factor: u32) -> GrayImage {
  let n_x = values.len() as u32;
  let n_y = values[0].len() as u32;

  let mut img = DynamicImage::new_luma8(n_x * scale_factor, n_y * scale_factor);
  let can = img.as_mut_luma8().unwrap();

  for x in 0..n_x {
    for y in 0..n_y {
      let p = Luma ( [values[x as usize][y as usize]] );
      for xi in 0..scale_factor {
        for yi in 0..scale_factor {
          can.put_pixel(x * scale_factor + xi, y * scale_factor + yi, p);
        }
      }
    }
  }

  img.to_luma8()
}

fn f32_to_complex(values: Vec<f32>) -> Vec<Complex<f32>> {
  values.into_iter().map(|v| {
    Complex::new(v, 0.0)
  }).collect()
}

fn complex_to_f32(values: Vec<Complex<f32>>) -> Vec<f32> {
  values.into_iter().map(|v| {
    v.re
  }).collect()
}

fn spatial_rep_to_pixels(spatial_representation: Vec<Vec<Complex<f32>>>) -> Vec<Vec<u8>> {
  spatial_representation.into_iter()
    .map(|values| complex_to_f32(values).into_iter()
      .map(|value| {
        (value * 256.0) as u8
      })
      .collect()
    )
    .collect()
}

fn prep_input(dirname: &str, dims: (u32, u32)) -> GrayImage {
  let img_original = ImageReader::open(INPUT_FILE).unwrap().decode().unwrap();
  let img_downsampled = img_original.resize(dims.0, dims.1, FilterType::Triangle);

  img_downsampled
    .resize(dims.0 * SCALE_FACTOR, dims.1 * SCALE_FACTOR, FilterType::Triangle)
    .save(&format!("{}/{}", dirname, INPUT_FILE_DOWNSAMPLED)).unwrap();

  img_downsampled.to_luma8()
}

fn reverse_ft_partial(momentums: &Vec<Vec<Complex<f32>>>, contributing: &Vec<(u32, u32)>) -> Vec<Vec<Complex<f32>>> {
  let n_x = momentums.len();
  let n_y = momentums[0].len();

  (0..n_x).into_iter().map(|x| {
    (0..n_y).into_iter().map(|y| {
      let fac_x = 2.0 * 3.14159 / (n_x as f32) * (x as f32);
      let fac_y = 2.0 * 3.14159 / (n_y as f32) * (y as f32);

      let mut sum = Complex::new(0.0, 0.0);

      for (kx, ky) in contributing {
        let f = momentums[*kx as usize][*ky as usize];
        sum += f * Complex::new(0.0, fac_x * (*kx as f32) + fac_y * (*ky as f32)).exp();
      }

      sum / ((n_x * n_y) as f32)
    }).collect()
  }).collect()
}

fn make_exploded_frequency_plots(dirname: &str, momentum: &Vec<Vec<Complex<f32>>>, cutoff: u32, cumulative: bool) {
  let bar = ProgressBar::new(cutoff as u64);
  bar.set_message("Computing partial reconstructions");
  bar.set_style(ProgressStyle::default_bar()
    .template("{msg} {bar:40.cyan/blue} [{eta} remaining]")
    .progress_chars("##-"));
  for kk in 0..cutoff {
    let mut contributing = Vec::new();

    for kx in 0..kk {
      for ky in 0..kk {
        if cumulative || kx == (kk - 1) || ky == (kk - 1) {
          contributing.push((kx, ky));
        }
      }
    }

    let pixels = spatial_rep_to_pixels(reverse_ft_partial(&momentum, &contributing));

    pixels_to_img(pixels, SCALE_FACTOR)
      .save(&format!("{}/exploded-{:02}.png", dirname, kk)).unwrap();
    bar.inc(1);
  }
  bar.finish();
}

fn run_analysis(dim: u32, freq_cutoff: u32, exploder: u32) {
  let dirname = format!("analysis-{}-{}-{}", dim, freq_cutoff, exploder);
  if !Path::new(&dirname).exists() {
    std::fs::create_dir(&dirname).unwrap();
  }

  let dims = (dim, dim);
  let cutoff = (freq_cutoff, freq_cutoff);

  let img = prep_input(&dirname, dims);

  let pixels = img_to_pixels(&img)
    .into_iter()
    .map(|values| f32_to_complex(values))
    .collect();
  let momentum_representation = forward_ft(&pixels, cutoff);

  make_exploded_frequency_plots(&dirname, &momentum_representation, exploder, false);

  let reconstructed_pixels = spatial_rep_to_pixels(reverse_ft(&momentum_representation));
  pixels_to_img(reconstructed_pixels, SCALE_FACTOR)
    .save(&format!("{}/test-reconstructed.png", dirname)).unwrap();
}

fn main() {
  run_analysis(DIM, FREQ_CUTOFF, EXPLODER_CUTOFF);
}

